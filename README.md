# E-commerce Cart - Spring + Hexagonal Architecture + CQRS in Java

E-commerce Cart it's a small java backend application project for an e-commerce build in Java 17.

## Pre-loaded fixed Data

Project have some fixed inserted registries in case you would like to play with the application.

```bash
Inserted products:
 - 1. UUID = '0dc12ca4-10cb-48bf-a365-4f93615b67ec'
 - 2. UUID = '7c2c65d3-cffb-4314-82b5-ee36c96240c1'

Inserted carts:
 - 1. UUID = '2fef0860-9a02-4337-aa67-25f209888c18'
 - 2. UUID = '56ea4e23-5c9f-4f8e-81a6-c30f64f1902d'
```

## Cucumber Acceptance Tests execution
**In order to execute the Cucumber acceptance tests use the following cli command**

```
./gradlew cucumberCli
```

**it will create an html file in the "target" folder with the tests execution report**

# API endpoints

List of the current application endpoints and it's behaviour.

## GET
`retrieves a product by it's id` [product/{id}](#)

**Response**

```
// ProductId exists
{
    "id": "0dc12ca4-10cb-48bf-a365-4f93615b67ec",
    "description": "TEST",
    "amount": 50
}
```

**Or**

```
// ProductId not exists
{
    "timestamp": "2023-01-23T11:29:16.1968666",
    "message": "Product with given id not found"
}
```

## GET
`retrieves all products` [products](#)

**Response**

```
// Fixed inserted products
[
    {
        "id": "0dc12ca4-10cb-48bf-a365-4f93615b67ec",
        "description": "A very fancy product",
        "amount": 50
    },
    {
        "id": "7c2c65d3-cffb-4314-82b5-ee36c96240c1",
        "description": "Not so fancy product but we still like it",
        "amount": 55
    }
]
```

## GET
`retrieves a cart by it's id` [cart/{id}](#)

**Response**

```
// CartId exists
{
    "id": "2fef0860-9a02-4337-aa67-25f209888c18",
    "productsCart": {
        "productCartList": []
    }
}
```

**Or**

```
// CartId not exists
{
    "timestamp": "2023-01-23T11:34:18.6048997",
    "message": "Cart with given id not found"
}
```

## POST
`creates a Product` [product](#)

**Given the following valid body**

```
{
    "description": "A new product",
    "amount": 100
}
```
**Returns empty response with 201 code status**

## PUT
`adds a Product to a cart and manage the amounts` [cart/:cartId/add/:productId?productAmount=:amount](#)

***Given existent cartId and productId***

```
cart/2fef0860-9a02-4337-aa67-25f209888c18/add/0dc12ca4-10cb-48bf-a365-4f93615b67ec?productAmount=5
```
**Returns empty response with 204 code status**
##

***Given not existent cartId and productId***

```
cart/44440860-9a02-4337-aa67-25f209888c18/add/0dc12ca4-10cb-48bf-a365-4f93615b67ec?productAmount=5
```
**It creates the cart with the given cartId and returns empty response with 204 code status**
##

***Given existent cartId but not existent productId***

```
cart/2fef0860-9a02-4337-aa67-25f209888c18/add/44442ca4-10cb-48bf-a365-4f93615b67ec?productAmount=5
```


```
{
    "timestamp": "2023-01-23T11:44:52.9387146",
    "message": "Product with given id not found"
}
```

## DELETE
`delete cart by it's id` [cart/id](#)

***Given existent cartId***

**Returns empty response with 204 code status**

##


***Given non existent cartId***

**Response**

```
// CartId not exists
{
    "timestamp": "2023-01-23T11:34:18.6048997",
    "message": "Cart with given id not found"
}
```


## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

Abel Vázquez