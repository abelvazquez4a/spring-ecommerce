package com.onebox.ecommerce.src.product.infrastructure.persistence;

import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.mother.product.ProductMother;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductIdStub;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    void given_validProduct_when_save_then_productIsSaved() {
        // Arrange
        Product product = ProductMother.random();

        // Act
        Product savedProduct = productRepository.save(product);

        // Assert
        Optional<Product> actualProduct = productRepository.findById(savedProduct.getProductId());
        assertTrue(actualProduct.isPresent());
        assertEquals(product.getDescription(), actualProduct.get().getDescription());
        assertEquals(product.getAmount().get(), actualProduct.get().getAmount().get());
    }

    @Test
    void given_invalidProductId_when_findById_then_returnEmpty() {
        // Arrange
        ProductId nonExistentProductId = ProductIdStub.random();

        // Act
        Optional<Product> actualProduct = productRepository.findById(nonExistentProductId);

        // Assert
        assertFalse(actualProduct.isPresent());
    }
}
