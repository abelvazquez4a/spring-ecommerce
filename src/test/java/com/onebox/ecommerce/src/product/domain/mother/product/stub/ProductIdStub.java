package com.onebox.ecommerce.src.product.domain.mother.product.stub;

import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;

import java.util.UUID;

public class ProductIdStub {
    public static ProductId create(String id){
        return new ProductId(UUID.fromString(id));
    }

    public static ProductId random(){
        return new ProductId();
    }
}
