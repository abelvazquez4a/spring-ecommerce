package com.onebox.ecommerce.src.product.application.create.createproduct.usecase;

import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.mother.product.ProductMother;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CreateProductUseCaseTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private CreateProductUseCase createProductUseCase;

    @Captor
    private ArgumentCaptor<Product> captor;

    @Test
    void given_validProduct_when_createProduct_then_saveProduct() {
        // Arrange
        Product expectedProduct = ProductMother.random();

        // Act
        createProductUseCase.createProduct(expectedProduct);

        // Assert
        verify(productRepository, times(1)).saveAndFlush(captor.capture());
        Product actualProduct = captor.getValue();
        assertEquals(expectedProduct, actualProduct);
    }
}