package com.onebox.ecommerce.src.product.domain.mother.product.stub;

import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescription;

import java.util.UUID;

public class ProductDescriptionStub {
    public static ProductDescription create(String description) {
        return new ProductDescription(description);
    }

    public static ProductDescription random(){
        return new ProductDescription(UUID.randomUUID().toString().replaceAll("_",""));
    }
}
