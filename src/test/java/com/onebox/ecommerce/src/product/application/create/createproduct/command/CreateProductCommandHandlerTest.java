package com.onebox.ecommerce.src.product.application.create.createproduct.command;

import com.onebox.ecommerce.src.product.application.create.createproduct.usecase.CreateProductUseCase;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescription;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CreateProductCommandHandlerTest {
    @Mock
    private CreateProductUseCase createProductUseCase;

    @InjectMocks
    private CreateProductCommandHandler createProductCommandHandler;

    @Captor
    private ArgumentCaptor<Product> captor;

    @Test
    void given_validCommand_whenHandle_then_createProduct() {
        // Arrange
        String description = "Test Product";
        int amount = 5;
        CreateProductCommand command = new CreateProductCommand(description, amount);
        ProductId          expectedProductId          = new ProductId();
        ProductDescription expectedProductDescription = new ProductDescription(description);
        ProductAmount      expectedProductAmount      = new ProductAmount(new AtomicInteger(amount));
        Product            expectedProduct            = new Product(expectedProductId, expectedProductDescription, expectedProductAmount);

        // Act
        createProductCommandHandler.handle(command);

        // Assert
        verify(createProductUseCase, times(1)).createProduct(captor.capture());
        Product actualProduct = captor.getValue();

        assertEquals(expectedProduct.getDescription(), actualProduct.getDescription());
        assertEquals(expectedProduct.getAmount().get(), actualProduct.getAmount().get());
    }
}