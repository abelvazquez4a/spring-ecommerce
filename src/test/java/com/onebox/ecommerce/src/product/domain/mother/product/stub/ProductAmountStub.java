package com.onebox.ecommerce.src.product.domain.mother.product.stub;

import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class ProductAmountStub {
    public static ProductAmount create(int amount) {
        return new ProductAmount(new AtomicInteger(amount));
    }

    public static ProductAmount random(){
        return new ProductAmount(new AtomicInteger(new Random().nextInt(Integer.MAX_VALUE)));
    }
}
