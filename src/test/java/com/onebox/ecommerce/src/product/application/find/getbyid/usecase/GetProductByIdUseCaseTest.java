package com.onebox.ecommerce.src.product.application.find.getbyid.usecase;

import com.onebox.ecommerce.src.product.domain.exception.ProductNotFoundException;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.mother.product.ProductMother;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductIdStub;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class GetProductByIdUseCaseTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private GetProductByIdUseCase getProductByIdUseCase;

    @Test
    void given_validProductId_when_getProductById_then_returnProduct() {
        // Arrange
        ProductId productId       = ProductIdStub.random();
        Product   expectedProduct = ProductMother.withId(productId);
        when(productRepository.findById(productId)).thenReturn(Optional.of(expectedProduct));

        // Act
        Product product = getProductByIdUseCase.getProductById(productId);

        // Assert
        assertEquals(expectedProduct, product);
        verify(productRepository, times(1)).findById(productId);
    }

    @Test
    void given_invalidProductId_when_getProductById_then_throwProductNotFoundException() {
        // Arrange
        ProductId productId = ProductIdStub.random();
        when(productRepository.findById(productId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ProductNotFoundException.class, () -> getProductByIdUseCase.getProductById(productId));
        verify(productRepository, times(1)).findById(productId);
    }

}
