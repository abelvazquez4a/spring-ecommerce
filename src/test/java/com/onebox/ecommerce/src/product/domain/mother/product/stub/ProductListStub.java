package com.onebox.ecommerce.src.product.domain.mother.product.stub;

import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.mother.product.ProductMother;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductListStub {

    public static List<Product> create(Product product){
        return Arrays.asList(product);
    }

    public static List<Product> randomProduct(){
        return Arrays.asList(ProductMother.random());
    }
    public static List<Product> randomWithGivenSize(int productListSize){
        List<Product> productList = new ArrayList<>(productListSize);
        for (int i = 0; i < productListSize; i++) {
            productList.add(i, ProductMother.random());
        }
        return productList;
    }

    public static List<Product> empty(){
        return new ArrayList<>();
    }
}
