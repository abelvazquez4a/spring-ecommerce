package com.onebox.ecommerce.src.product.application.update.updateamount.usecase;

import com.onebox.ecommerce.src.cart.domain.exception.InsufficientStockException;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.mother.product.ProductMother;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductAmountStub;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UpdateProductAmountUseCaseTest {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private UpdateProductAmountUseCase updateProductAmountUseCase;

    @Test
    void given_validProduct_when_updateProductAmount_then_updateAmount() {
        // Arrange
        Product       product         = ProductMother.random();
        ProductAmount newAmount       = ProductAmountStub.create(product.getAmount().get() - 2);
        Product       expectedProduct = ProductMother.withGivenIdAndAmount(product.getProductId(), newAmount);

        // Act
        updateProductAmountUseCase.updateProductAmount(product, 2);

        // Assert
        ArgumentCaptor<Product> captor = ArgumentCaptor.forClass(Product.class);
        verify(productRepository, times(1)).save(captor.capture());
        Product actualProduct = captor.getValue();

        assertEquals(expectedProduct.getAmount().get(), actualProduct.getAmount().get());
    }

    @Test
    void given_insufficientStock_when_updateProductAmount_then_throwInsufficientStockException() {
        // Arrange
        Product product = ProductMother.random();
        int amountToDecrement = product.getAmount().get() + 1;

        // Act and Assert
        assertThrows(InsufficientStockException.class, () -> updateProductAmountUseCase.updateProductAmount(product, amountToDecrement));
    }
}