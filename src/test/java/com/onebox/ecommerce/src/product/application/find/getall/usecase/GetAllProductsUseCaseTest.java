package com.onebox.ecommerce.src.product.application.find.getall.usecase;

import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductListStub;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class GetAllProductsUseCaseTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private GetAllProductsUseCase getAllProductsUseCase;

    @Test
    void given_validRepository_when_getAllProducts_then_returnListOfProducts() {
        // Arrange
        List<Product> expectedProducts = ProductListStub.randomWithGivenSize(5);
        when(productRepository.findAll()).thenReturn(expectedProducts);

        // Act
        List<Product> products = getAllProductsUseCase.getAllProducts();

        // Assert
        assertEquals(expectedProducts, products);
        verify(productRepository, times(1)).findAll();
    }
}