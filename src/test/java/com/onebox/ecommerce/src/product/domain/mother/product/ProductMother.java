package com.onebox.ecommerce.src.product.domain.mother.product;

import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductAmountStub;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductDescriptionStub;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductIdStub;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescription;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;

public class ProductMother {
    public static Product create(ProductId productId, ProductDescription productDescription, ProductAmount productAmount){
        return new Product(productId,productDescription,productAmount);
    }

    public static Product withId(ProductId productId){
        return create(productId, ProductDescriptionStub.random(), ProductAmountStub.random());
    }

    public static Product withGivenIdAndAmount(ProductId productId, ProductAmount productAmount){
        return create(productId, ProductDescriptionStub.random(), productAmount);
    }

    public static Product random(){
        return create(ProductIdStub.random(), ProductDescriptionStub.random(), ProductAmountStub.random());
    }
}
