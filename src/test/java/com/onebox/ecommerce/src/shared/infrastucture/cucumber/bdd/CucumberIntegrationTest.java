package com.onebox.ecommerce.src.shared.infrastucture.cucumber.bdd;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/com/onebox/ecommerce/application/acceptance")
public class CucumberIntegrationTest {

}
