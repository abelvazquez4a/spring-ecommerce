package com.onebox.ecommerce.src.shared.infrastucture.cucumber.bdd;


import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CucumberSpringConfiguration {
    public final TestRestTemplate testRestTemplate;

    public CucumberSpringConfiguration(TestRestTemplate testRestTemplate) {
        this.testRestTemplate = testRestTemplate;
    }
}
