package com.onebox.ecommerce.src.shared.infrastucture.cucumber.bdd.step;

import com.onebox.ecommerce.src.cart.application.find.getbyid.response.GetCartByIdResponse;
import com.onebox.ecommerce.src.product.application.create.createproduct.command.CreateProductCommand;
import com.onebox.ecommerce.src.product.application.find.getall.response.GetAllProductResponse;
import com.onebox.ecommerce.src.product.application.find.getbyid.response.GetProductByIdResponse;
import com.onebox.ecommerce.src.shared.infrastucture.cucumber.bdd.CucumberSpringConfiguration;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

@CucumberContextConfiguration
public class ApiContext extends CucumberSpringConfiguration {

    public ApiContext(TestRestTemplate testRestTemplate) {
        super(testRestTemplate);
    }

    public ResponseEntity response;

    @When("the client calls \\/product giving a product with the following description: {string} and the following amount: {int}")
    public void theClientCallsProductGivingAProductWithTheFollowingDescriptionAndTheFollowingAmount(String description, int amount) {
        response = testRestTemplate.postForEntity("/product", new CreateProductCommand(description,amount), CreateProductCommand.class);
    }

    @Then("the client receives status code of {int}")
    public void theClientReceivesStatusCodeOf(int statusCode) {
        Assertions.assertEquals(statusCode, response.getStatusCode().value());
    }

    @When("the client calls GET \\/products$")
    public void theClientCallsProducts() {
        response = testRestTemplate.getForEntity("/products", GetAllProductResponse[].class);
    }

    @When("the client calls GET \\/product with the following id: {string}")
    public void theClientCallsGETProductWithTheFollowingId(String id) {
        response = testRestTemplate.getForEntity("/product/{id}", GetProductByIdResponse.class, id);
    }


    @When("the client calls GET \\/cart with the following id: {string}")
    public void theClientCallsGETCartWithTheFollowingId(String id) {
        response = testRestTemplate.getForEntity("/cart/{id}", GetCartByIdResponse.class, id);
    }

    @When("the client calls DELETE \\/cart with the following id: {string}")
    public void theClientCallsDELETECartWithTheFollowingId(String id) {
        response = testRestTemplate.exchange("/cart/{id}", HttpMethod.DELETE, null, Void.class, id);
    }

    @When("the client calls PUT \\/cart\\/\\{cartId}\\/add\\/\\{productId} with the following cartId: {string} and the following productId: {string} and the following product amount: {int}")
    public void theClientCallsPUTCartCartIdAddProductIdWithTheFollowingCartIdAndTheFollowingProductId(
            String cartId,
            String productId,
            int productAmount
                                                                                                     ) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString("/cart/{cartId}/add/{productId}")
                .queryParam("productAmount", productAmount);
        response = testRestTemplate.exchange(builder.buildAndExpand(cartId, productId).toUri(), HttpMethod.PUT, null, Void.class);
    }
}
