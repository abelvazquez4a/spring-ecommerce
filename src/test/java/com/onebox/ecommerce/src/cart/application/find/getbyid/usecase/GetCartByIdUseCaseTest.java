package com.onebox.ecommerce.src.cart.application.find.getbyid.usecase;

import com.onebox.ecommerce.src.cart.domain.exception.CartNotFoundException;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.mother.cart.CartMother;
import com.onebox.ecommerce.src.cart.domain.mother.cart.stub.CartIdStub;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GetCartByIdUseCaseTest {

    @Mock
    private CartRepository cartRepository;

    @InjectMocks
    private GetCartByIdUseCase getCartByIdUseCase;

    @Test
    void given_validCartId_whenGetCartById_then_returnCart() {
        // Arrange
        CartId cartId = CartIdStub.random();
        Cart expectedCart = CartMother.random();
        when(cartRepository.findById(cartId)).thenReturn(expectedCart);

        // Act
        Cart cart = getCartByIdUseCase.getCartById(cartId);

        // Assert
        assertEquals(expectedCart, cart);
        verify(cartRepository, times(1)).findById(cartId);
    }

    @Test()
    void given_invalidCartId_whenGetCartById_then_throwException() {
        // Arrange
        CartId cartId = CartIdStub.random();
        when(cartRepository.findById(cartId)).thenReturn(null);

        //Act & Assert
        assertThrows(CartNotFoundException.class, () -> getCartByIdUseCase.getCartById(cartId));
    }
}