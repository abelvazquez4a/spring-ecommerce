package com.onebox.ecommerce.src.cart.domain.mother.cart;

import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.model.CartProduct;
import com.onebox.ecommerce.src.cart.domain.mother.cart.stub.CartIdStub;
import com.onebox.ecommerce.src.cart.domain.mother.cart.stub.CartProductListStub;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;

import java.util.List;

public class CartMother {

    public static Cart create(CartId cartId, List<CartProduct> cartProductList){
        return new Cart(cartId, cartProductList);
    }

    public static Cart withId(CartId cartId){
        return create(cartId, CartProductListStub.randomProduct());
    }

    public static Cart random(){
        return create(CartIdStub.random(), CartProductListStub.randomProduct());
    }

    public static Cart withIdAndWithoutProducts(CartId cartId){
        return create(cartId, CartProductListStub.empty());
    }

    public static Cart withGivenCartProduct(CartProduct product){
        return create(CartIdStub.random(),CartProductListStub.create(product));
    }
}
