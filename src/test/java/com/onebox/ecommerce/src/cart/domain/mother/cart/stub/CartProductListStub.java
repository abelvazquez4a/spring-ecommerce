package com.onebox.ecommerce.src.cart.domain.mother.cart.stub;

import com.onebox.ecommerce.src.cart.domain.model.CartProduct;
import com.onebox.ecommerce.src.cart.domain.mother.cartproduct.CartProductMother;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CartProductListStub {

    public static List<CartProduct> create(CartProduct product){
        return Arrays.asList(product);
    }

    public static List<CartProduct> randomProduct(){
        return Arrays.asList(CartProductMother.random());
    }
    public static List<CartProduct> randomWithGivenSize(int productListSize){
        List<CartProduct> productList = new ArrayList<>(productListSize);
        for (int i = 0; i < productListSize; i++) {
            productList.add(i, CartProductMother.random());
        }
        return productList;
    }

    public static List<CartProduct> empty(){
        return new ArrayList<>();
    }

}
