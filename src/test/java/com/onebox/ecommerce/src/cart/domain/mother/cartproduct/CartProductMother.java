package com.onebox.ecommerce.src.cart.domain.mother.cartproduct;

import com.onebox.ecommerce.src.cart.domain.model.CartProduct;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductAmountStub;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductDescriptionStub;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductIdStub;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescription;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;

public class CartProductMother {
    public static CartProduct create(ProductId productId, ProductDescription productDescription, ProductAmount productAmount){
        return new CartProduct(productId,productDescription,productAmount);
    }

    public static CartProduct withId(ProductId productId){
        return create(productId, ProductDescriptionStub.random(), ProductAmountStub.random());
    }

    public static CartProduct random(){
        return create(ProductIdStub.random(), ProductDescriptionStub.random(), ProductAmountStub.random());
    }
}
