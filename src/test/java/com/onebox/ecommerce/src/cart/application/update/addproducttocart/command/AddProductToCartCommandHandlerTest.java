package com.onebox.ecommerce.src.cart.application.update.addproducttocart.command;

import com.onebox.ecommerce.src.cart.application.find.getbyid.usecase.GetCartByIdUseCase;
import com.onebox.ecommerce.src.cart.application.update.addproducttocart.usecase.AddProductToCartUseCase;
import com.onebox.ecommerce.src.cart.domain.exception.CartNotFoundException;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.mother.cart.CartMother;
import com.onebox.ecommerce.src.cart.domain.mother.cart.stub.CartIdStub;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import com.onebox.ecommerce.src.product.application.find.getbyid.usecase.GetProductByIdUseCase;
import com.onebox.ecommerce.src.product.domain.exception.ProductNotFoundException;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.mother.product.ProductMother;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductIdStub;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AddProductToCartCommandHandlerTest {

    @Mock
    private AddProductToCartUseCase addProductToCartUseCase;

    @Mock
    private GetProductByIdUseCase getProductByIdUseCase;

    @Mock
    private GetCartByIdUseCase getCartByIdUseCase;

    @InjectMocks
    private AddProductToCartCommandHandler addProductToCartCommandHandler;

    @Test
    void given_validCommand_whenHandle_then_callUseCase() {
        // Arrange
        ProductId               productId = ProductIdStub.random();
        CartId                  cartId    = CartIdStub.random();
        Product                 product   = ProductMother.random();
        Cart                    cart      = CartMother.random();
        when(getProductByIdUseCase.getProductById(productId)).thenReturn(product);
        when(getCartByIdUseCase.getCartById(cartId)).thenReturn(cart);
        AddProductToCartCommand command   = new AddProductToCartCommand(cartId.getId().toString(), productId.getId().toString(), 2);

        // Act
        addProductToCartCommandHandler.handle(command);

        // Assert
        verify(addProductToCartUseCase, times(1)).addProductToCart(product, cart, command.getProductAmount());
    }

    @Test
    void given_validProductId_whenHandle_then_addProductToCart() {
        // Assert
        ProductId productId = ProductIdStub.random();
        CartId cartId = CartIdStub.random();
        AddProductToCartCommand command = new AddProductToCartCommand(cartId.getId().toString(), productId.getId().toString(), 1);
        Product expectedProduct = ProductMother.withId(productId);
        Cart expectedCart = CartMother.withId(cartId);
        when(getProductByIdUseCase.getProductById(any(ProductId.class))).thenReturn(expectedProduct);
        when(getCartByIdUseCase.getCartById(any(CartId.class))).thenReturn(expectedCart);

        // Act
        addProductToCartCommandHandler.handle(command);

        // Assert
        verify(getProductByIdUseCase, times(1)).getProductById(productId);
        verify(getCartByIdUseCase, times(1)).getCartById(cartId);
        verify(addProductToCartUseCase, times(1)).addProductToCart(expectedProduct, expectedCart, 1);
    }

    @Test
    void given_invalidProductId_whenHandle_then_throwProductNotFoundException() {
        // Assert
        ProductId productId = ProductIdStub.random();
        CartId cartId = CartIdStub.random();
        AddProductToCartCommand command = new AddProductToCartCommand(cartId.getId().toString(), productId.getId().toString(), 1);
        when(getProductByIdUseCase.getProductById(any(ProductId.class))).thenThrow(ProductNotFoundException.class);

        // Act and Assert
        assertThrows(ProductNotFoundException.class, () -> addProductToCartCommandHandler.handle(command));
        verify(getProductByIdUseCase, times(1)).getProductById(productId);
        verify(getCartByIdUseCase, never()).getCartById(cartId);
        verify(addProductToCartUseCase, never()).addProductToCart(any(Product.class), any(Cart.class), anyInt());
    }

    @Test
    void given_validProductId_and_invalidCartId_whenHandle_then_createCart() {
        // Assert
        ProductId productId = ProductIdStub.random();
        CartId    cartId    = CartIdStub.random();
        AddProductToCartCommand command = new AddProductToCartCommand(
                cartId.getId().toString(),
                productId.getId().toString(),
                1
        );
        Product expectedProduct = ProductMother.withId(productId);
        Cart    expectedCart    = CartMother.withIdAndWithoutProducts(cartId);
        when(getProductByIdUseCase.getProductById(any(ProductId.class))).thenReturn(expectedProduct);
        when(getCartByIdUseCase.getCartById(any(CartId.class))).thenThrow(CartNotFoundException.class);

        // Act
        addProductToCartCommandHandler.handle(command);

        // Assert
        verify(getProductByIdUseCase, times(1)).getProductById(productId);
        verify(getCartByIdUseCase, times(1)).getCartById(cartId);
        verify(addProductToCartUseCase, times(1)).addProductToCart(expectedProduct, expectedCart, 1);
    }
}