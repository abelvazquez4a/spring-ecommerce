package com.onebox.ecommerce.src.cart.application.delete.deletebyid.usecase;

import com.onebox.ecommerce.src.cart.application.find.getbyid.usecase.GetCartByIdUseCase;
import com.onebox.ecommerce.src.cart.domain.exception.CartNotFoundException;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.mother.cart.CartMother;
import com.onebox.ecommerce.src.cart.domain.mother.cart.stub.CartIdStub;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import com.onebox.ecommerce.src.product.application.find.getbyid.usecase.GetProductByIdUseCase;
import com.onebox.ecommerce.src.product.application.update.updateamount.usecase.UpdateProductAmountUseCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DeleteCartByIdUseCaseTest {

    @Mock
    private CartRepository             cartRepository;
    @Mock
    private GetProductByIdUseCase      getProductByIdUseCase;
    @Mock
    private UpdateProductAmountUseCase updateProductAmountUseCase;
    @Mock
    private GetCartByIdUseCase         getCartByIdUseCase;

    @InjectMocks
    private DeleteCartByIdUseCase deleteCartByIdUseCase;


    @Test
    void deleteById_shouldCallRepositoryDelete() throws CartNotFoundException {
        // Arrange
        CartId cartId = CartIdStub.random();
        Cart   cart   = CartMother.random();
        when(getCartByIdUseCase.getCartById(cartId)).thenReturn(cart);

        // Act
        deleteCartByIdUseCase.deleteById(cartId);

        // Assert
        verify(cartRepository).deleteById(cartId);
    }

    @Test
    void deleteById_shouldThrowCartNotFoundException_whenCartNotExists() {
        // Arrange
        CartId cartId = CartIdStub.random();
        when(getCartByIdUseCase.getCartById(cartId)).thenThrow(new CartNotFoundException());

        // Act and Assert
        assertThrows(CartNotFoundException.class, () -> deleteCartByIdUseCase.deleteById(cartId));
    }
}
