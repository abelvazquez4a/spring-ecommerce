package com.onebox.ecommerce.src.cart.application.update.updatecartproductamount.usecase;

import com.onebox.ecommerce.src.cart.domain.exception.CartProductNotFoundException;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.model.CartProduct;
import com.onebox.ecommerce.src.cart.domain.mother.cart.CartMother;
import com.onebox.ecommerce.src.cart.domain.mother.cartproduct.CartProductMother;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.product.domain.mother.product.stub.ProductIdStub;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.stereotype.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@Service
class UpdateCartProductAmountUseCaseTest {

    @Mock
    private CartRepository cartRepository;

    @InjectMocks
    private UpdateCartProductAmountUseCase updateCartProductAmountUseCase;

    @Test
    void given_validCartAndProductId_when_updateCartProductAmount_then_updateAmount() {
        // Arrange
        CartProduct cartProduct = CartProductMother.random();
        Cart      cart      = CartMother.withGivenCartProduct(cartProduct);
        int       newAmount = 2;

        // Act
        updateCartProductAmountUseCase.updateCartProductAmount(cart, cartProduct.getProductId(), newAmount);

        // Assert
        assertEquals(newAmount, cart.getCartProductById(cartProduct.getProductId()).get().getAmount().get());
        verify(cartRepository, times(1)).save(cart);
    }

    @Test
    void given_validCartAndInvalidProductId_when_updateCartProductAmount_then_throwException() {
        // Arrange
        Cart cart = CartMother.random();
        ProductId productId = ProductIdStub.random();
        int newAmount = 2;

        // Act & Assert
        assertThrows(CartProductNotFoundException.class, () -> updateCartProductAmountUseCase.updateCartProductAmount(cart, productId, newAmount));
    }
}
