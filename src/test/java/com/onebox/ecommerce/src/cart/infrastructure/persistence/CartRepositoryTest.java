package com.onebox.ecommerce.src.cart.infrastructure.persistence;

import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.mother.cart.CartMother;
import com.onebox.ecommerce.src.cart.domain.mother.cart.stub.CartIdStub;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import com.onebox.ecommerce.src.cart.infrastructure.repository.CaffeineCartRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CartRepositoryTest {
    private CaffeineCartRepositoryImpl cartRepository;
    private CartId                     cartId;
    private Cart                       cart;

    @BeforeEach
    public void setUp() {
        cartRepository = new CaffeineCartRepositoryImpl();
        cartId         = CartIdStub.random();
        cart       = CartMother.withId(cartId);
    }

    @Test
    void shouldSaveCart() {
        //Act
        cartRepository.save(cart);

        //Assert
        assertEquals(cartRepository.findById(cartId), cart);
    }

    @Test
    void shouldDeleteCart() {
        // Arrange
        cartRepository.save(cart);

        // Act
        cartRepository.deleteById(cartId);

        // Assert
        assertNull(cartRepository.findById(cartId));
    }

    @Test
    void shouldGetCartWhenExists() {
        // Arrange
        cartRepository.save(cart);

        // Act
        Cart cart = cartRepository.findById(cartId);

        // Assert
        assertNotNull(cart);
    }

    @Test
    void shouldGetNullCartWhenCartNotExists() {
        //Arrange
        CartId cartId = CartIdStub.random();

        // Act
        Cart cart = cartRepository.findById(cartId);

        // Assert
        assertNull(cart);
    }
}