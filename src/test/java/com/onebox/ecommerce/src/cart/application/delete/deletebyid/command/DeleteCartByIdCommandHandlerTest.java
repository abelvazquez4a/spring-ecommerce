package com.onebox.ecommerce.src.cart.application.delete.deletebyid.command;

import com.onebox.ecommerce.src.cart.application.delete.deletebyid.usecase.DeleteCartByIdUseCase;
import com.onebox.ecommerce.src.cart.domain.exception.CartNotFoundException;
import com.onebox.ecommerce.src.cart.domain.mother.cart.stub.CartIdStub;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DeleteCartByIdCommandHandlerTest {
    @Mock
    private DeleteCartByIdUseCase deleteCartByIdUseCase;

    @InjectMocks
    private DeleteCartByIdCommandHandler deleteCartByIdCommandHandler;

    @Test
    void givenExistentCart_whenHandle_thenShouldCall_useCaseDeleteById() throws Exception {
        // Arrange
        CartId cartId = CartIdStub.random();
        DeleteCartByIdCommand command = new DeleteCartByIdCommand(cartId.getId().toString());

        // Act
        deleteCartByIdCommandHandler.handle(command);

        // Assert
        verify(deleteCartByIdUseCase, times(1)).deleteById(cartId);
    }

    @Test
    void givenNonExistentCart_whenHandle_thenThrowCartNotFoundException() {
        // Arrange
        CartId cartId = CartIdStub.random();
        DeleteCartByIdCommand command = new DeleteCartByIdCommand(cartId.getId().toString());
        doThrow(new CartNotFoundException()).when(deleteCartByIdUseCase).deleteById(any(CartId.class));

        //Act & Assert
        assertThrows(CartNotFoundException.class, () -> deleteCartByIdCommandHandler.handle(command));
    }
}