package com.onebox.ecommerce.src.cart.domain.mother.cart.stub;

import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;

import java.util.UUID;

public final class CartIdStub {
    public static CartId create(String id){
        return new CartId(UUID.fromString(id));
    }

    public static CartId random(){
        return new CartId();
    }
}
