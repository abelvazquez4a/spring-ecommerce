Feature: the user gets a Cart by id

  Scenario: client makes call to GET /cart with a non existent CartId
    When the client calls GET /cart with the following id: "fa9a9e6d-2de1-47cf-bc73-2b9b8ac231b6"
    Then the client receives status code of 404

  Scenario: client makes call to GET /cart with an existent CartId
    When the client calls GET /cart with the following id: "2fef0860-9a02-4337-aa67-25f209888c18"
    Then the client receives status code of 200