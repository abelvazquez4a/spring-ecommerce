Feature: the user creates a Product

  Scenario: client makes call to POST /product with a valid Product
    When the client calls /product giving a product with the following description: "TEST" and the following amount: 5
    Then the client receives status code of 201

  Scenario: client makes call to POST /product with an invalid ProductDescription
    When the client calls /product giving a product with the following description: "" and the following amount: 5
    Then the client receives status code of 400

  Scenario: client makes call to POST /product with an invalid ProductAmount
    When the client calls /product giving a product with the following description: "TEST" and the following amount: -1
    Then the client receives status code of 400