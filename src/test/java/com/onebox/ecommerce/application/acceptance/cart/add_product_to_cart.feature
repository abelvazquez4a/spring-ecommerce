Feature: the user adds a Product to a Cart

  Scenario: client makes call to PUT /cart/{cartId}/add/{productId} with a non existent CartId, it creates the Cart with given ID
    When the client calls PUT /cart/{cartId}/add/{productId} with the following cartId: "9adf5c7c-d17a-440e-9ad2-96ae6639e400" and the following productId: "0dc12ca4-10cb-48bf-a365-4f93615b67ec" and the following product amount: 5
    Then the client receives status code of 204

  Scenario: client makes call to PUT /cart/{cartId}/add/{productId} with a non existent ProductId
    When the client calls PUT /cart/{cartId}/add/{productId} with the following cartId: "2fef0860-9a02-4337-aa67-25f209888c18" and the following productId: "1ca74ae8-2c67-4c17-be76-d6729ad1504c" and the following product amount: 5
    Then the client receives status code of 404

  Scenario: client makes call to PUT /cart/{cartId}/add/{productId} with an existent CartId and ProductId
    When the client calls PUT /cart/{cartId}/add/{productId} with the following cartId: "2fef0860-9a02-4337-aa67-25f209888c18" and the following productId: "0dc12ca4-10cb-48bf-a365-4f93615b67ec" and the following product amount: 5
    Then the client receives status code of 204

  Scenario: client makes call to PUT /cart/{cartId}/add/{productId} with an existent CartId and ProductId but not sufficient stock
    When the client calls PUT /cart/{cartId}/add/{productId} with the following cartId: "2fef0860-9a02-4337-aa67-25f209888c18" and the following productId: "0dc12ca4-10cb-48bf-a365-4f93615b67ec" and the following product amount: 500
    Then the client receives status code of 409