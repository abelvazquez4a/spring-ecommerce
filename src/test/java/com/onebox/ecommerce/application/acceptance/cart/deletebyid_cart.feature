Feature: the user deletes a Cart by id

  Scenario: client makes call to DELETE /cart with a non existent CartId
    When the client calls DELETE /cart with the following id: "2d5e18c1-195f-467e-9966-24bca4e0caa8"
    Then the client receives status code of 404

  Scenario: client makes call to DELETE /cart with an existent CartId
    When the client calls DELETE /cart with the following id: "56ea4e23-5c9f-4f8e-81a6-c30f64f1902d"
    Then the client receives status code of 204