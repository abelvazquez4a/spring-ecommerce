Feature: the user gets a Product by id

  Scenario: client makes call to GET /product with a non existent ProductId
    When the client calls GET /product with the following id: "9adf5c7c-d17a-440e-9ad2-96ae6639e400"
    Then the client receives status code of 404

  Scenario: client makes call to GET /product with an existent ProductId
    When the client calls GET /product with the following id: "7c2c65d3-cffb-4314-82b5-ee36c96240c1"
    Then the client receives status code of 200