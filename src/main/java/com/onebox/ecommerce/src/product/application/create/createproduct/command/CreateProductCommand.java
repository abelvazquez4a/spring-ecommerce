package com.onebox.ecommerce.src.product.application.create.createproduct.command;

import com.onebox.ecommerce.shared.domain.commandbus.Command;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class CreateProductCommand extends Command {
    String description;
    int amount;
}
