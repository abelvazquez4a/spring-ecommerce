package com.onebox.ecommerce.src.product.domain.vo.productamount;

import com.onebox.ecommerce.src.product.domain.exception.InvalidProductAmountException;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public record ProductAmount(AtomicInteger amount) {
    public ProductAmount {
        checkAmountNotNegativeOrNull(amount);
    }

    private void checkAmountNotNegativeOrNull(AtomicInteger amount) {
        if (amount == null || amount.get() < 0) {
            throw new InvalidProductAmountException();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductAmount that)) {
            return false;
        }
        return amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }
}
