package com.onebox.ecommerce.src.product.application.find.getall.query;

import com.onebox.ecommerce.shared.domain.querybus.QueryHandler;
import com.onebox.ecommerce.src.product.application.find.getall.response.GetAllProductResponse;
import com.onebox.ecommerce.src.product.application.find.getall.response.GetAllProductResponseConverter;
import com.onebox.ecommerce.src.product.application.find.getall.usecase.GetAllProductsUseCase;
import com.onebox.ecommerce.src.product.domain.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetAllProductsQueryHandler implements QueryHandler<List<GetAllProductResponse>, GetAllProductsQuery> {

    private final GetAllProductsUseCase getAllProductsUseCase;

    @Autowired
    public GetAllProductsQueryHandler(GetAllProductsUseCase getAllProductsUseCase) {
        this.getAllProductsUseCase = getAllProductsUseCase;
    }

    @Override
    public List<GetAllProductResponse> handle(GetAllProductsQuery query) {
        List<Product>               productList                     = this.getAllProductsUseCase.getAllProducts();
        List<GetAllProductResponse> getAllProductsQueryResponseList = new ArrayList<>();
        productList.forEach(product -> getAllProductsQueryResponseList.add(GetAllProductResponseConverter.convert(product)));
        return getAllProductsQueryResponseList;
    }

}
