package com.onebox.ecommerce.src.product.domain.vo.productdescription;

import javax.persistence.AttributeConverter;

public class ProductDescriptionAttributeConverter implements AttributeConverter<ProductDescription, String> {
    @Override
    public String convertToDatabaseColumn(ProductDescription attribute) {
        return attribute.description();
    }

    @Override
    public ProductDescription convertToEntityAttribute(String dbData) {
        return new ProductDescription(dbData);
    }
}
