package com.onebox.ecommerce.src.product.infrastructure.adapter.find.getall;

import com.onebox.ecommerce.shared.domain.ApiController;
import com.onebox.ecommerce.shared.domain.commandbus.CommandBus;
import com.onebox.ecommerce.shared.domain.querybus.QueryBus;
import com.onebox.ecommerce.src.product.application.find.getall.query.GetAllProductsQuery;
import com.onebox.ecommerce.src.product.application.find.getall.response.GetAllProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GetAllProductsController extends ApiController {
    @Autowired
    public GetAllProductsController(QueryBus queryBus, CommandBus commandBus) {
        super(queryBus, commandBus);
    }

    @GetMapping("products")
    public ResponseEntity<List<GetAllProductResponse>> getAllProducts() throws Exception {
        List<GetAllProductResponse> getAllProductsQueryResponseList = ask(new GetAllProductsQuery());
        return new ResponseEntity<>(getAllProductsQueryResponseList,HttpStatus.OK);
    }
}
