package com.onebox.ecommerce.src.product.domain.vo.productdescription;

import com.onebox.ecommerce.src.product.domain.exception.InvalidProductDescriptionException;

import java.util.Objects;

public record ProductDescription(String description) {
    public ProductDescription {
        validateDescriptionIsDefined(description);
    }

    private void validateDescriptionIsDefined(String description) {
        if (description == null || description.isEmpty()) {
            throw new InvalidProductDescriptionException();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductDescription that)) {
            return false;
        }
        return description.equals(that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }
}
