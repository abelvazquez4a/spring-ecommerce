package com.onebox.ecommerce.src.product.application.find.getall.response;

import com.onebox.ecommerce.src.product.domain.model.Product;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GetAllProductResponseConverter {
    public static GetAllProductResponse convert(Product product){
        return new GetAllProductResponse(product.getId(), product.getDescription(), product.getAmount().get());
    }
}
