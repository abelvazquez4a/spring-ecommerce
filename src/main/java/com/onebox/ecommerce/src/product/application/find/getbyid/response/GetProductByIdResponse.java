package com.onebox.ecommerce.src.product.application.find.getbyid.response;

public record GetProductByIdResponse(String id, String description, int amount){
}
