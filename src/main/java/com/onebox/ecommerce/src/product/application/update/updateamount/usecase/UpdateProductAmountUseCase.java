package com.onebox.ecommerce.src.product.application.update.updateamount.usecase;

import com.onebox.ecommerce.src.cart.domain.exception.InsufficientStockException;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateProductAmountUseCase {
    private final ProductRepository productRepository;

    @Autowired
    public UpdateProductAmountUseCase(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void updateProductAmount(Product product, int productAmountToDecrement){
        if(product.getAmount().addAndGet(-productAmountToDecrement) < 0){
            throw new InsufficientStockException();
        }
        this.productRepository.save(product);
    }
}
