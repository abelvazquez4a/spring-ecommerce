package com.onebox.ecommerce.src.product.infrastructure.adapter.create;

import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescription;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
public class CreateProductCLR {
    @Bean
    public CommandLineRunner createProduct(ProductRepository productRepository) {
        return args -> {
            ProductId productId = new ProductId(UUID.fromString("0dc12ca4-10cb-48bf-a365-4f93615b67ec"));
            ProductId anotherProductId = new ProductId(UUID.fromString("7c2c65d3-cffb-4314-82b5-ee36c96240c1"));
            ProductDescription productDescription = new ProductDescription("A very fancy product");
            ProductAmount productAmount = new ProductAmount(new AtomicInteger(50));
            productRepository.saveAndFlush(new Product(productId, productDescription, productAmount));
            ProductDescription anotherProductDescription = new ProductDescription("Not so fancy product but we still like it");
            ProductAmount anotherProductAmount = new ProductAmount(new AtomicInteger(55));
            productRepository.saveAndFlush(new Product(anotherProductId, anotherProductDescription, anotherProductAmount));
        };
    }
}
