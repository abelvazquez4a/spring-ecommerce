package com.onebox.ecommerce.src.product.application.find.getbyid.response;

import com.onebox.ecommerce.src.product.domain.model.Product;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GetProductByIdResponseConverter {
    public static GetProductByIdResponse convert(Product product){
        return new GetProductByIdResponse(product.getId(), product.getDescription(), product.getAmount().get());
    }
}
