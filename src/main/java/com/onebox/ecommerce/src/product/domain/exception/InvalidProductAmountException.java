package com.onebox.ecommerce.src.product.domain.exception;

public class InvalidProductAmountException extends RuntimeException{
    public InvalidProductAmountException(){
        super("Given product amount is not valid");
    }
}
