package com.onebox.ecommerce.src.product.application.find.getbyid.usecase;

import com.onebox.ecommerce.src.product.domain.exception.ProductNotFoundException;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GetProductByIdUseCase {
    private final ProductRepository productRepository;

    public GetProductByIdUseCase(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product getProductById(ProductId productId){
        return guard(productRepository.findById(productId));
    }

    private Product guard(Optional<Product> productOptional){
        if(productOptional.isEmpty()){
            throw new ProductNotFoundException();
        }
        return productOptional.get();
    }
}
