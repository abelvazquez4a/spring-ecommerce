package com.onebox.ecommerce.src.product.domain.exception;

public class InvalidProductIdException extends RuntimeException{
    public InvalidProductIdException(){
        super("Given product id is not valid");
    }
}
