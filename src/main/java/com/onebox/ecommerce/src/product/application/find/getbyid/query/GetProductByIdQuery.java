package com.onebox.ecommerce.src.product.application.find.getbyid.query;

import com.onebox.ecommerce.shared.domain.querybus.Query;
import com.onebox.ecommerce.src.product.application.find.getbyid.response.GetProductByIdResponse;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Getter
@Value
public class GetProductByIdQuery extends Query<GetProductByIdResponse> {
    String id;
}
