package com.onebox.ecommerce.src.product.application.create.createproduct.command;

import com.onebox.ecommerce.shared.domain.commandbus.CommandHandler;
import com.onebox.ecommerce.src.product.application.create.createproduct.usecase.CreateProductUseCase;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescription;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class CreateProductCommandHandler implements CommandHandler<CreateProductCommand> {

    private final CreateProductUseCase createProductUseCase;

    @Autowired
    public CreateProductCommandHandler(CreateProductUseCase createProductUseCase) {
        this.createProductUseCase = createProductUseCase;
    }

    @Override
    public void handle(CreateProductCommand command) {
        ProductId productId = new ProductId();
        ProductDescription productDescription = new ProductDescription(command.getDescription());
        ProductAmount productAmount = new ProductAmount(new AtomicInteger(command.getAmount()));
        Product product = new Product(productId, productDescription, productAmount);
        createProductUseCase.createProduct(product);
    }
}
