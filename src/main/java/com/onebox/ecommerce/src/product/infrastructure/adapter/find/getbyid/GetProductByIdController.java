package com.onebox.ecommerce.src.product.infrastructure.adapter.find.getbyid;

import com.onebox.ecommerce.shared.domain.ApiController;
import com.onebox.ecommerce.shared.domain.commandbus.CommandBus;
import com.onebox.ecommerce.shared.domain.querybus.QueryBus;
import com.onebox.ecommerce.src.product.application.find.getbyid.query.GetProductByIdQuery;
import com.onebox.ecommerce.src.product.application.find.getbyid.response.GetProductByIdResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetProductByIdController extends ApiController {
    protected GetProductByIdController(QueryBus queryBus, CommandBus commandBus) {
        super(queryBus, commandBus);
    }

    @GetMapping("product/{id}")
    public ResponseEntity<GetProductByIdResponse> getProductById(@PathVariable String id) throws Exception {
        GetProductByIdResponse getProductByIdResponse = ask(new GetProductByIdQuery(id));
        return new ResponseEntity<>(getProductByIdResponse, HttpStatus.OK);
    }
}
