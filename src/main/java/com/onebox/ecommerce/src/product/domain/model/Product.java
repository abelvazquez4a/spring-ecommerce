package com.onebox.ecommerce.src.product.domain.model;

import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmountAttributeConverter;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescription;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescriptionAttributeConverter;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @EmbeddedId
    @Column(name="id", updatable = false, nullable = false)
    private ProductId productId;

    @Column(name = "description")
    @Convert(converter = ProductDescriptionAttributeConverter.class)
    private ProductDescription productDescription;

    @Column(name = "amount")
    @Convert(converter = ProductAmountAttributeConverter.class)
    private ProductAmount productAmount;

    public String getId(){
        return productId.getId().toString();
    }

    public String getDescription(){
        return productDescription.description();
    }

    public AtomicInteger getAmount(){
        return this.productAmount.amount();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product product)) {
            return false;
        }
        return productId.equals(product.productId) && productDescription.equals(product.productDescription) && productAmount.equals(
                product.productAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productDescription, productAmount);
    }
}
