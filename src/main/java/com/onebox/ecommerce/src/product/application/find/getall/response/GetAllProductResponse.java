package com.onebox.ecommerce.src.product.application.find.getall.response;

public record GetAllProductResponse(String id, String description, int amount) {
}
