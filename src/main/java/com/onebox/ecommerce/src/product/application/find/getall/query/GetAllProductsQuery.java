package com.onebox.ecommerce.src.product.application.find.getall.query;

import com.onebox.ecommerce.shared.domain.querybus.Query;
import com.onebox.ecommerce.src.product.application.find.getall.response.GetAllProductResponse;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Value
public class GetAllProductsQuery extends Query<List<GetAllProductResponse>> {
}
