package com.onebox.ecommerce.src.product.domain.vo.productamount;

import javax.persistence.AttributeConverter;
import java.util.concurrent.atomic.AtomicInteger;

public class ProductAmountAttributeConverter implements AttributeConverter<ProductAmount, Integer> {
    @Override
    public Integer convertToDatabaseColumn(ProductAmount attribute) {
        return attribute.amount().get();
    }

    @Override
    public ProductAmount convertToEntityAttribute(Integer dbData) {
        return new ProductAmount(new AtomicInteger(dbData));
    }
}
