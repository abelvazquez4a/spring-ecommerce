package com.onebox.ecommerce.src.product.application.create.createproduct.usecase;

import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateProductUseCase {

    private final ProductRepository productRepository;

    @Autowired
    public CreateProductUseCase(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void createProduct(Product product){
        this.productRepository.saveAndFlush(product);
    }
}
