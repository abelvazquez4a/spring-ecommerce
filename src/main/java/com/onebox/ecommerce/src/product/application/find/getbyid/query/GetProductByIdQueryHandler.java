package com.onebox.ecommerce.src.product.application.find.getbyid.query;

import com.onebox.ecommerce.shared.domain.querybus.QueryHandler;
import com.onebox.ecommerce.src.product.application.find.getbyid.response.GetProductByIdResponse;
import com.onebox.ecommerce.src.product.application.find.getbyid.response.GetProductByIdResponseConverter;
import com.onebox.ecommerce.src.product.application.find.getbyid.usecase.GetProductByIdUseCase;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class GetProductByIdQueryHandler implements QueryHandler<GetProductByIdResponse, GetProductByIdQuery> {

    private final GetProductByIdUseCase getProductByIdUseCase;

    public GetProductByIdQueryHandler(GetProductByIdUseCase getProductByIdUseCase) {
        this.getProductByIdUseCase = getProductByIdUseCase;
    }

    @Override
    public GetProductByIdResponse handle(GetProductByIdQuery query) {
        ProductId productId = new ProductId(UUID.fromString(query.getId()));
        return GetProductByIdResponseConverter.convert(getProductByIdUseCase.getProductById(productId));
    }
}
