package com.onebox.ecommerce.src.product.infrastructure.adapter.create;

import com.onebox.ecommerce.shared.domain.ApiController;
import com.onebox.ecommerce.shared.domain.commandbus.CommandBus;
import com.onebox.ecommerce.shared.domain.querybus.QueryBus;
import com.onebox.ecommerce.src.product.application.create.createproduct.command.CreateProductCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CreateProductController extends ApiController {
    @Autowired
    public CreateProductController(QueryBus queryBus, CommandBus commandBus) {
        super(queryBus, commandBus);
    }

    @PostMapping("product")
    public ResponseEntity<Void> createProduct(@RequestBody CreateProductCommand createProductCommand) throws Exception {
        dispatch(createProductCommand);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
