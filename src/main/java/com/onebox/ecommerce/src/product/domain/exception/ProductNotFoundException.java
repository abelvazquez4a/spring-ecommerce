package com.onebox.ecommerce.src.product.domain.exception;

public class ProductNotFoundException extends RuntimeException{
    public ProductNotFoundException(){
        super("Product with given id not found");
    }
}
