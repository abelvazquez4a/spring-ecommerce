package com.onebox.ecommerce.src.product.domain.exception;

public class InvalidProductDescriptionException extends RuntimeException{
    public InvalidProductDescriptionException(){
        super("Given product description is not valid");
    }
}
