package com.onebox.ecommerce.src.cart.application.dto;

import com.onebox.ecommerce.src.cart.domain.model.CartProduct;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CartProductResponseConverter {
    public static CartProductResponse convert(CartProduct product){
        return new CartProductResponse(product.getId(), product.getDescription(), product.getAmount().get());
    }
}
