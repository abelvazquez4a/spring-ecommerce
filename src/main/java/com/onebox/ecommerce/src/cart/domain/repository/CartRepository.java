package com.onebox.ecommerce.src.cart.domain.repository;

import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;

public interface CartRepository {
    Cart findById(CartId cartId);

    void save(Cart cart);

    void deleteById(CartId cartId);
}
