package com.onebox.ecommerce.src.cart.application.find.getbyid.response;

import com.onebox.ecommerce.src.cart.application.dto.CartProductsResponseConverter;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GetCartByIdResponseConverter {
    public static GetCartByIdResponse convert(Cart cart){
        return new GetCartByIdResponse(cart.getId(), CartProductsResponseConverter.convert(cart.getCartProductList()));
    }
}
