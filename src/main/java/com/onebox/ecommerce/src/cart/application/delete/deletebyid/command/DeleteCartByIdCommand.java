package com.onebox.ecommerce.src.cart.application.delete.deletebyid.command;

import com.onebox.ecommerce.shared.domain.commandbus.Command;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class DeleteCartByIdCommand extends Command {
    String id;
}
