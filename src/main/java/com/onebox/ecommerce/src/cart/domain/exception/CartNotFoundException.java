package com.onebox.ecommerce.src.cart.domain.exception;

public class CartNotFoundException extends RuntimeException{
    public CartNotFoundException(){
        super("Cart with given id not found");
    }
}
