package com.onebox.ecommerce.src.cart.application.update.addproducttocart.command;

import com.onebox.ecommerce.shared.domain.commandbus.Command;
import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public class AddProductToCartCommand extends Command {
    String cartId;
    String productId;
    int productAmount;
}
