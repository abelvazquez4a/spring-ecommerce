package com.onebox.ecommerce.src.cart.infrastructure.adapter.delete.deletebyid;

import com.onebox.ecommerce.shared.domain.ApiController;
import com.onebox.ecommerce.shared.domain.commandbus.CommandBus;
import com.onebox.ecommerce.shared.domain.querybus.QueryBus;
import com.onebox.ecommerce.src.cart.application.delete.deletebyid.command.DeleteCartByIdCommand;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeleteCartByIdController extends ApiController {
    public DeleteCartByIdController(QueryBus queryBus, CommandBus commandBus) {
        super(queryBus, commandBus);
    }

    @DeleteMapping("cart/{id}")
    public ResponseEntity<Void> deleteCartById(@PathVariable String id) throws Exception {
        dispatch(new DeleteCartByIdCommand(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
