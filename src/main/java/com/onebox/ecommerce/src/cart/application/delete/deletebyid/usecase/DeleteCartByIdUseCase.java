package com.onebox.ecommerce.src.cart.application.delete.deletebyid.usecase;

import com.onebox.ecommerce.src.cart.application.find.getbyid.usecase.GetCartByIdUseCase;
import com.onebox.ecommerce.src.cart.domain.exception.CartNotFoundException;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import com.onebox.ecommerce.src.product.application.find.getbyid.usecase.GetProductByIdUseCase;
import com.onebox.ecommerce.src.product.application.update.updateamount.usecase.UpdateProductAmountUseCase;
import com.onebox.ecommerce.src.product.domain.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteCartByIdUseCase {

    private final CartRepository cartRepository;
    private final GetProductByIdUseCase getProductByIdUseCase;
    private final UpdateProductAmountUseCase updateProductAmountUseCase;
    private final GetCartByIdUseCase getCartByIdUseCase;

    @Autowired
    public DeleteCartByIdUseCase(CartRepository cartRepository,
                                 GetProductByIdUseCase getProductByIdUseCase,
                                 UpdateProductAmountUseCase updateProductAmountUseCase,
                                 GetCartByIdUseCase getCartByIdUseCase
                                 ) {
        this.cartRepository = cartRepository;
        this.getProductByIdUseCase = getProductByIdUseCase;
        this.updateProductAmountUseCase = updateProductAmountUseCase;
        this.getCartByIdUseCase = getCartByIdUseCase;
    }

    public void deleteById(CartId cartId) throws CartNotFoundException {
        Cart cart = getCartByIdUseCase.getCartById(cartId);
        cart.getCartProductList().forEach(cartProduct -> {
            Product product = getProductByIdUseCase.getProductById(cartProduct.getProductId());
            updateProductAmountUseCase.updateProductAmount(product, cartProduct.getAmount().get() * -1);
        });
        cartRepository.deleteById(cartId);
    }
}
