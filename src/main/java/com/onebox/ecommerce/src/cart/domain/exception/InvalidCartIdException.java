package com.onebox.ecommerce.src.cart.domain.exception;

public class InvalidCartIdException extends RuntimeException{
    public InvalidCartIdException(){
        super("Given cart id is not valid");
    }
}
