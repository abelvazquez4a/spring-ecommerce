package com.onebox.ecommerce.src.cart.infrastructure.adapter.create;

import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.UUID;

@Configuration
public class CreateCartCLR {
    @Bean
    public CommandLineRunner createCart(CartRepository cartRepository) {
        return args -> {
            CartId    cartId        = new CartId(UUID.fromString("2fef0860-9a02-4337-aa67-25f209888c18"));
            CartId anotherCartId = new CartId(UUID.fromString("56ea4e23-5c9f-4f8e-81a6-c30f64f1902d"));
            cartRepository.save(new Cart(cartId, new ArrayList<>()));
            cartRepository.save(new Cart(anotherCartId, new ArrayList<>()));
        };
    }
}