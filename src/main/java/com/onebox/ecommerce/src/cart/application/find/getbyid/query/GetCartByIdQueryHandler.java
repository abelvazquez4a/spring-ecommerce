package com.onebox.ecommerce.src.cart.application.find.getbyid.query;

import com.onebox.ecommerce.shared.domain.querybus.QueryHandler;
import com.onebox.ecommerce.src.cart.application.find.getbyid.response.GetCartByIdResponse;
import com.onebox.ecommerce.src.cart.application.find.getbyid.response.GetCartByIdResponseConverter;
import com.onebox.ecommerce.src.cart.application.find.getbyid.usecase.GetCartByIdUseCase;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class GetCartByIdQueryHandler implements QueryHandler<GetCartByIdResponse, GetCartByIdQuery> {

    private final GetCartByIdUseCase getCartByIdUseCase;

    @Autowired
    public GetCartByIdQueryHandler(GetCartByIdUseCase getCartByIdUseCase) {
        this.getCartByIdUseCase = getCartByIdUseCase;
    }

    @Override
    public GetCartByIdResponse handle(GetCartByIdQuery query) {
        CartId cartId = new CartId(UUID.fromString(query.getId()));
        return GetCartByIdResponseConverter.convert(getCartByIdUseCase.getCartById(cartId));
    }
}
