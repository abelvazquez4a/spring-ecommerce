package com.onebox.ecommerce.src.cart.infrastructure.repository;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import io.reactivex.subjects.PublishSubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.concurrent.TimeUnit;

@Repository
public class CaffeineCartRepositoryImpl implements CartRepository {

    private final Cache<CartId, Cart> cartCache;

    private final PublishSubject<Cart> cartExpirationObservable = PublishSubject.create();


    @Autowired
    public CaffeineCartRepositoryImpl() {
        cartCache = Caffeine.newBuilder()
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .removalListener((key, value, cause) -> {
                    if (cause == RemovalCause.EXPIRED) {
                        this.cartExpirationObservable.onNext((Cart)value);
                    }
                })
                .build();
    }

    public PublishSubject<Cart> getCartExpirationObservable(){
        return cartExpirationObservable;
    }

    @Override
    public Cart findById(CartId cartId) {
        return cartCache.getIfPresent(cartId);
    }

    @Override
    public void save(Cart cart) {
        cartCache.put(cart.getCartId(), cart);
    }

    @Override
    public void deleteById(CartId cartId) {
        cartCache.invalidate(cartId);
    }
}
