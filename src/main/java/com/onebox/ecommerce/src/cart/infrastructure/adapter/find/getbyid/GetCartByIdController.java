package com.onebox.ecommerce.src.cart.infrastructure.adapter.find.getbyid;

import com.onebox.ecommerce.shared.domain.ApiController;
import com.onebox.ecommerce.shared.domain.commandbus.CommandBus;
import com.onebox.ecommerce.shared.domain.querybus.QueryBus;
import com.onebox.ecommerce.src.cart.application.find.getbyid.query.GetCartByIdQuery;
import com.onebox.ecommerce.src.cart.application.find.getbyid.response.GetCartByIdResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetCartByIdController extends ApiController {
    public GetCartByIdController(QueryBus queryBus, CommandBus commandBus) {
        super(queryBus, commandBus);
    }

    @GetMapping("cart/{id}")
    public ResponseEntity<GetCartByIdResponse> getCartById(@PathVariable String id) throws Exception {
        GetCartByIdResponse getCartByIdResponse = ask(new GetCartByIdQuery(id));
        return new ResponseEntity<>(getCartByIdResponse, HttpStatus.OK);
    }
}
