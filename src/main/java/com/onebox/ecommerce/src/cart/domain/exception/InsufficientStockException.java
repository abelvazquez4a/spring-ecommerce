package com.onebox.ecommerce.src.cart.domain.exception;

public class InsufficientStockException extends RuntimeException{
    public InsufficientStockException(){
        super("Insufficient stock for the given product");
    }
}
