package com.onebox.ecommerce.src.cart.application.find.getbyid.query;

import com.onebox.ecommerce.shared.domain.querybus.Query;
import com.onebox.ecommerce.src.cart.application.find.getbyid.response.GetCartByIdResponse;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Getter
@Value
public class GetCartByIdQuery extends Query<GetCartByIdResponse> {
    String id;
}
