package com.onebox.ecommerce.src.cart.application.find.getbyid.usecase;

import com.onebox.ecommerce.src.cart.domain.exception.CartNotFoundException;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import org.springframework.stereotype.Service;

@Service
public class GetCartByIdUseCase {

    private final CartRepository cartRepository;

    public GetCartByIdUseCase(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public Cart getCartById(CartId cartId){
        return guard(cartRepository.findById(cartId));
    }

    private Cart guard(Cart cart){
        if(cart == null){
            throw new CartNotFoundException();
        }
        return cart;
    }
}
