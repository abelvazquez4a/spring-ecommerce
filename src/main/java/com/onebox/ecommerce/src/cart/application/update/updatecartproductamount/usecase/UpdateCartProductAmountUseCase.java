package com.onebox.ecommerce.src.cart.application.update.updatecartproductamount.usecase;

import com.onebox.ecommerce.src.cart.domain.exception.CartProductNotFoundException;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.model.CartProduct;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UpdateCartProductAmountUseCase {

    private final CartRepository cartRepository;

    @Autowired
    public UpdateCartProductAmountUseCase(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    public void updateCartProductAmount(Cart cart, ProductId productId, int productAmount){
        CartProduct cartProduct = guard(cart.getCartProductById(productId));
        cartProduct.getAmount().set(productAmount);
        cartRepository.save(cart);
    }

    private CartProduct guard(Optional<CartProduct> cartProductOpt){
        if(cartProductOpt.isEmpty()){
            throw new CartProductNotFoundException();
        }
        return cartProductOpt.get();
    }
}
