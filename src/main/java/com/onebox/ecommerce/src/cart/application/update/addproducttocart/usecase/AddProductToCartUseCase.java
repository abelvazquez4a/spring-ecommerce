package com.onebox.ecommerce.src.cart.application.update.addproducttocart.usecase;

import com.onebox.ecommerce.src.cart.application.update.updatecartproductamount.usecase.UpdateCartProductAmountUseCase;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.model.CartProduct;
import com.onebox.ecommerce.src.cart.domain.repository.CartRepository;
import com.onebox.ecommerce.src.product.application.update.updateamount.usecase.UpdateProductAmountUseCase;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Transactional
public class AddProductToCartUseCase {

    private final CartRepository cartRepository;
    private final UpdateProductAmountUseCase updateProductAmountUseCase;
    private final UpdateCartProductAmountUseCase updateCartProductAmountUseCase;

    @Autowired
    public AddProductToCartUseCase(CartRepository cartRepository,
                                   UpdateProductAmountUseCase updateProductAmountUseCase,
                                   UpdateCartProductAmountUseCase updateCartProductAmountUseCase
                                   ) {
        this.cartRepository = cartRepository;
        this.updateProductAmountUseCase = updateProductAmountUseCase;
        this.updateCartProductAmountUseCase = updateCartProductAmountUseCase;
    }

    public void addProductToCart(Product product, Cart cart, int productAmount){
        Optional<CartProduct> cartProductOpt = cart.getCartProductById(product.getProductId());
        int cartProductAmount = productAmount;
        CartProduct cartProduct;
        if(cartProductOpt.isPresent()){
            cartProduct = cartProductOpt.get();
            productAmount = productAmount - cartProduct.getAmount().get();
        } else {
            cartProduct = new CartProduct(product.getProductId(), product.getProductDescription(), new ProductAmount(new AtomicInteger(productAmount)));
            cart.getCartProductList().add(cartProduct);
        }
        updateProductAmountUseCase.updateProductAmount(product, productAmount);
        updateCartProductAmountUseCase.updateCartProductAmount(cart, product.getProductId(), cartProductAmount);
        this.cartRepository.save(cart);
    }
}
