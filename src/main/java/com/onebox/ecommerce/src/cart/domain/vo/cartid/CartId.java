package com.onebox.ecommerce.src.cart.domain.vo.cartid;

import com.onebox.ecommerce.shared.domain.ValueObject;
import com.onebox.ecommerce.src.product.domain.exception.InvalidProductIdException;
import lombok.Getter;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Type;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
@Immutable
@Getter
public class CartId implements ValueObject, Serializable {
    @Type(type="org.hibernate.type.UUIDCharType")
    UUID id;

    public CartId(){
        this.id = UUID.randomUUID();
    }

    public CartId(UUID id){
        validateIdIsNotNull(id);
        this.id = id;
    }

    private void validateIdIsNotNull(UUID id){
        if(id == null){
            throw new InvalidProductIdException();
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CartId productId = (CartId) o;
        return id.equals(productId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "CartId{" +
                "id=" + id +
                '}';
    }
}
