package com.onebox.ecommerce.src.cart.application.find.getbyid.response;

import com.onebox.ecommerce.src.cart.application.dto.CartProductsResponse;

public record GetCartByIdResponse(String id, CartProductsResponse productsCart) {
}
