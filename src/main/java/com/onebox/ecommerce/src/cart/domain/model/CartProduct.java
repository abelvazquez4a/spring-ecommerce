package com.onebox.ecommerce.src.cart.domain.model;

import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmount;
import com.onebox.ecommerce.src.product.domain.vo.productamount.ProductAmountAttributeConverter;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescription;
import com.onebox.ecommerce.src.product.domain.vo.productdescription.ProductDescriptionAttributeConverter;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.Convert;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@AllArgsConstructor
public class CartProduct {

    private ProductId productId;

    @Convert(converter = ProductDescriptionAttributeConverter.class)
    private ProductDescription productDescription;

    @Convert(converter = ProductAmountAttributeConverter.class)
    private ProductAmount productAmount;

    public String getId(){
        return productId.getId().toString();
    }

    public String getDescription(){
        return productDescription.description();
    }

    public AtomicInteger getAmount(){
        return this.productAmount.amount();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CartProduct that)) {
            return false;
        }
        return productId.equals(that.productId) && productDescription.equals(that.productDescription) && productAmount.equals(
                that.productAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productDescription, productAmount);
    }
}
