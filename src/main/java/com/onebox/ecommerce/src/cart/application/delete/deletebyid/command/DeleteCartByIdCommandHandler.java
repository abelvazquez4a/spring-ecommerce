package com.onebox.ecommerce.src.cart.application.delete.deletebyid.command;

import com.onebox.ecommerce.shared.domain.commandbus.CommandHandler;
import com.onebox.ecommerce.src.cart.application.delete.deletebyid.usecase.DeleteCartByIdUseCase;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DeleteCartByIdCommandHandler implements CommandHandler<DeleteCartByIdCommand> {

    private final DeleteCartByIdUseCase deleteCartByIdUseCase;

    public DeleteCartByIdCommandHandler(DeleteCartByIdUseCase deleteCartByIdUseCase) {
        this.deleteCartByIdUseCase = deleteCartByIdUseCase;
    }

    @Override
    public void handle(DeleteCartByIdCommand command) throws Exception {
        CartId cartId = new CartId(UUID.fromString(command.getId()));
        deleteCartByIdUseCase.deleteById(cartId);
    }
}
