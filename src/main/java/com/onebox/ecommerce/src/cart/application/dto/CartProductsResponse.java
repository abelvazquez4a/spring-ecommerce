package com.onebox.ecommerce.src.cart.application.dto;

import java.util.List;

public record CartProductsResponse(List<CartProductResponse> productCartList) {
}
