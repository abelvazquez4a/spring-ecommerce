package com.onebox.ecommerce.src.cart.domain.model;

import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Cart {
    private CartId cartId;

    private List<CartProduct> cartProductList = new ArrayList<>();

    public String getId(){
        return cartId.getId().toString();
    }

    public Optional<CartProduct> getCartProductById(ProductId productId){
        return cartProductList.stream().filter(p -> p.getProductId().getId().equals(productId.getId())).findFirst();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cart cart)) {
            return false;
        }
        return cartId.equals(cart.cartId) && cartProductList.equals(cart.cartProductList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cartId, cartProductList);
    }
}
