package com.onebox.ecommerce.src.cart.application.update.addproducttocart.command;

import com.onebox.ecommerce.shared.domain.commandbus.CommandHandler;
import com.onebox.ecommerce.src.cart.application.find.getbyid.usecase.GetCartByIdUseCase;
import com.onebox.ecommerce.src.cart.application.update.addproducttocart.usecase.AddProductToCartUseCase;
import com.onebox.ecommerce.src.cart.domain.exception.CartNotFoundException;
import com.onebox.ecommerce.src.cart.domain.model.Cart;
import com.onebox.ecommerce.src.cart.domain.vo.cartid.CartId;
import com.onebox.ecommerce.src.product.application.find.getbyid.usecase.GetProductByIdUseCase;
import com.onebox.ecommerce.src.product.domain.model.Product;
import com.onebox.ecommerce.src.product.domain.vo.productid.ProductId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.UUID;

@Component
public class AddProductToCartCommandHandler implements CommandHandler<AddProductToCartCommand> {

    private final AddProductToCartUseCase addProductToCartUseCase;
    private final GetProductByIdUseCase   getProductByIdUseCase;
    private final GetCartByIdUseCase getCartByIdUseCase;

    @Autowired
    public AddProductToCartCommandHandler(AddProductToCartUseCase addProductToCartUseCase,
                                          GetProductByIdUseCase getProductByIdUseCase,
                                          GetCartByIdUseCase getCartByIdUseCase
                                          ) {
        this.addProductToCartUseCase = addProductToCartUseCase;
        this.getProductByIdUseCase   = getProductByIdUseCase;
        this.getCartByIdUseCase      = getCartByIdUseCase;
    }

    @Override
    public void handle(AddProductToCartCommand command) {
        ProductId productId = new ProductId(UUID.fromString(command.getProductId()));
        CartId cartId = new CartId(UUID.fromString(command.getCartId()));
        Product product = getProductByIdUseCase.getProductById(productId);
        Cart cart = createIfDoesntExists(cartId);
        this.addProductToCartUseCase.addProductToCart(product, cart, command.getProductAmount());
    }

    private Cart createIfDoesntExists(CartId cartId){
        Cart cart;
        try{
            cart = getCartByIdUseCase.getCartById(cartId);
        } catch (CartNotFoundException e){
            //If the cart doesnt exists we create it, maybe we can improve this and separate cart creation responsability in another scenario.
            cart = new Cart(cartId, new ArrayList<>());
        }
        return cart;
    }
}
