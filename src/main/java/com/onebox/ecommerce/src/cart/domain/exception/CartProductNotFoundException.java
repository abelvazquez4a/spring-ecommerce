package com.onebox.ecommerce.src.cart.domain.exception;

public class CartProductNotFoundException extends RuntimeException{
    public CartProductNotFoundException(){
        super("CartProduct with given id not found in the given Cart");
    }
}
