package com.onebox.ecommerce.src.cart.infrastructure.listener;

import com.onebox.ecommerce.src.cart.infrastructure.repository.CaffeineCartRepositoryImpl;
import com.onebox.ecommerce.src.product.application.find.getbyid.usecase.GetProductByIdUseCase;
import com.onebox.ecommerce.src.product.application.update.updateamount.usecase.UpdateProductAmountUseCase;
import com.onebox.ecommerce.src.product.domain.model.Product;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CartExpirationObservable {

    private final CaffeineCartRepositoryImpl caffeineCartRepository;
    private final GetProductByIdUseCase      getProductByIdUseCase;
    private final UpdateProductAmountUseCase updateProductAmountUseCase;

    public CartExpirationObservable(CaffeineCartRepositoryImpl caffeineCartRepository,
                                    GetProductByIdUseCase getProductByIdUseCase,
                                    UpdateProductAmountUseCase updateProductAmountUseCase
                                    ) {
        this.caffeineCartRepository     = caffeineCartRepository;
        this.getProductByIdUseCase      = getProductByIdUseCase;
        this.updateProductAmountUseCase = updateProductAmountUseCase;
        this.subscribeToCartCacheChanges();
    }

    private void subscribeToCartCacheChanges() {
        caffeineCartRepository.getCartExpirationObservable().subscribe(cart -> {
            cart.getCartProductList().forEach(cartProduct -> {
                Product product = getProductByIdUseCase.getProductById(cartProduct.getProductId());
                updateProductAmountUseCase.updateProductAmount(product, cartProduct.getAmount().get() * -1);
            });
        });
    }
}
