package com.onebox.ecommerce.src.cart.application.dto;

import com.onebox.ecommerce.src.cart.domain.model.CartProduct;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CartProductsResponseConverter {
    public static CartProductsResponse convert(List<CartProduct> productList){
        List<CartProductResponse> productCartList = new ArrayList<>();
        productList.forEach(product -> productCartList.add(CartProductResponseConverter.convert(product)));
        return new CartProductsResponse(productCartList);
    }
}
