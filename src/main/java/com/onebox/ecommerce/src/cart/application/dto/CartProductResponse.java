package com.onebox.ecommerce.src.cart.application.dto;

public record CartProductResponse(String id, String description, int amount) {
}
