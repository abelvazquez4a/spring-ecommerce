package com.onebox.ecommerce.src.cart.infrastructure.adapter.update.addproducttocart;

import com.onebox.ecommerce.shared.domain.ApiController;
import com.onebox.ecommerce.shared.domain.commandbus.CommandBus;
import com.onebox.ecommerce.shared.domain.querybus.QueryBus;
import com.onebox.ecommerce.src.cart.application.update.addproducttocart.command.AddProductToCartCommand;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddProductToCartController extends ApiController {
    public AddProductToCartController(QueryBus queryBus, CommandBus commandBus) {
        super(queryBus, commandBus);
    }

    @PutMapping("cart/{cartId}/add/{productId}")
    public ResponseEntity<Void> addProductToCart(@PathVariable String cartId, @PathVariable String productId, @RequestParam int productAmount) throws Exception {
        dispatch(new AddProductToCartCommand(cartId, productId, productAmount));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
