package com.onebox.ecommerce.shared.domain;

import com.onebox.ecommerce.shared.domain.commandbus.Command;
import com.onebox.ecommerce.shared.domain.commandbus.CommandBus;
import com.onebox.ecommerce.shared.domain.querybus.Query;
import com.onebox.ecommerce.shared.domain.querybus.QueryBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class ApiController {

    private final QueryBus queryBus;
    private final CommandBus commandBus;


    @Autowired
    protected ApiController(QueryBus queryBus, CommandBus commandBus) {
        this.queryBus = queryBus;
        this.commandBus = commandBus;
    }

    public <T> T ask(Query query) throws Exception {
        return (T) this.queryBus.ask(query);
    }

    public void dispatch(Command command) throws Exception {
        this.commandBus.dispatch(command);
    }
}
