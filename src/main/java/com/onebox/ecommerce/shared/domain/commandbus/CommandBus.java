package com.onebox.ecommerce.shared.domain.commandbus;

public interface CommandBus {
    void dispatch(Command command) throws Exception;
}
