package com.onebox.ecommerce.shared.domain.querybus;

public interface QueryBus {

    <T> T ask(Query<T> query) throws Exception;
}